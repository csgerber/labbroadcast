package edu.uchicago.gerber.labbroadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by macbook on 5/25/17.
 */
//https://stackoverflow.com/questions/15698790/broadcast-receiver-for-checking-internet-connection-in-android-app
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {


            Toast.makeText(context, getNetworkStatus(context).toString(),Toast.LENGTH_LONG).show();


    }


    //just get the state of the network.
    public NetworkInfo getNetworkStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();

    }






    //checking connectivity


    boolean checkInternet(Context context) {

        if (isNetworkAvailable(context)) {
            return true;
        } else {
            return false;
        }
    }


    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }




}